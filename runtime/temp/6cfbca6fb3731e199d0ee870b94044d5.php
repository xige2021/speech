<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:82:"C:\Users\50278\Desktop\peiyin\public/../application/index\view\index\tutorial.html";i:1641968474;s:72:"C:\Users\50278\Desktop\peiyin\application\index\view\layout\default.html";i:1641974481;s:69:"C:\Users\50278\Desktop\peiyin\application\index\view\common\meta.html";i:1641930899;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
<title><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?> – <?php echo htmlentities($site['name']); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<?php if(isset($keywords)): ?>
<meta name="keywords" content="<?php echo htmlentities($keywords); ?>">
<?php endif; if(isset($description)): ?>
<meta name="description" content="<?php echo htmlentities($description); ?>">
<?php endif; ?>

<link rel="shortcut icon" href="/assets/img/favicon.ico" />

<!--<link href="/assets/css/frontend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo htmlentities(\think\Config::get('site.version')); ?>" rel="stylesheet">-->

<link rel="shortcut icon" href="/assets/img/favicon.ico"/>
<!-- Bootstrap Core CSS -->
<link href="/assets/libs/bootstrap/dist/css/bootstrap.min.css?v=<?php echo htmlentities(\think\Config::get('site.version')); ?>" rel="stylesheet">
<link href="/assets/css/index.css?v=<?php echo htmlentities(\think\Config::get('site.version')); ?>" rel="stylesheet">

<link href="/static/app/api/font-awesome-4.7.0/css/font-awesome.css?v=<?php echo htmlentities(\think\Config::get('site.version')); ?>" rel="stylesheet">
<link href="/static/app/api/layui-v2.6.8/css/layui.css?v=<?php echo htmlentities(\think\Config::get('site.version')); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config: <?php echo json_encode($config); ?>,
        site:<?php echo json_encode($site); ?>,
    };
</script>

</head>

<body>
<div class="_nav">
    <nav class="navTabs-box">
        <div class="navTabs" style="padding-top: 25px;line-height: 25px;">
            <a href="/" class="mugua-site">
                <img src="<?php echo htmlentities($site['logo']); ?>" class="logo" alt="LOGO">
                <div class="title">
                    <span><?php echo $site['name']; ?></span>
                    <small>自媒体声音专家</small>
                </div>
            </a>
            <span class="right-txt">
               <?php echo $site['content']; ?>
            </span>
            <div style="clear: both;"></div>
        </div>
        <div class="navTabs-Right">
            <span class="tabs-mb fa fa-align-justify"></span>

            <ul class="nav-pc nav navbar-nav">
                <li><a href="<?php echo url('/'); ?>">首页</a></li>
                <li><a href="<?php echo url('index/index/tutorial'); ?>" title="教程">教程</a></li>
                <?php if(!$user): ?>
                <li><a href="<?php echo url('index/user/login'); ?>">登录</a></li>
                <li><a href="<?php echo url('index/user/register'); ?>">注册</a></li>
                <?php endif; if($user): ?>
                <li><a href="<?php echo url('index/user/task'); ?>" title="配音下载">配音下载</a></li>
                <li><a href="<?php echo url('index/user/index'); ?>"><?php echo $user['email']; ?></a></li>
                <li><a href="<?php echo url('index/user/logout'); ?>"><?php echo __('Sign out'); ?></a></li>
                <?php endif; ?>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </nav>
</div>
    <main class="content">
        <div id="app" class="container-fluid" style="margin-bottom: 50px;max-width: 1080px;">
    <div class="_header"></div>
    <div class="_content" id="page-main">
        <div class="task-form task-container" v-cloak>
            <audio controls id="voice-player" style="display: none;"></audio>
            <audio controls id="audition-player" style="display: none;"></audio>
            <div class="task-right">
                <section class="voice-select">
                    <header class="header">
                        <i class="fa fa-user"></i> <span class="text">选择发音人：{{voicer}}</span>
                    </header>
                    <div class="content-box" id="choose-voice">
                        <div class="select-bar">
                            <ul>
                                <li v-for="g in voiceGroup" v-bind:class="{ active: g.id == curGroup }"
                                    v-on:click="changeGroup(g)">{{g.name}}
                                </li>
                            </ul>
                        </div>
                        <div class="select-content scrollbar">
                            <ul v-for="g in voiceGroup" v-show="g.id == curGroup"
                                v-bind:class="{currentGroup: g.id == curGroup}">
                                <li v-for="v in list" v-show="v.category_id == curGroup"
                                    v-bind:class="{active: v.name == voicer , currentVoice:v.name == voicer}"
                                    v-on:click="changeVoice(v)" v-on:dblclick="playVoice(v)">
                                    <h4>
                                        <span class="fa fa-play-circle player player-voice" v-if="v.name != playing"
                                              data-toggle="tooltip" data-placement="right" title="点击试听"
                                              v-on:click="playVoice(v)"></span>
                                        <span class="fa fa-pause-circle player playing" v-if="v.name == playing"
                                              data-toggle="tooltip" data-placement="right" title="点击试听"
                                              v-on:click="playVoice(v)"></span>
                                        {{v.name}}
                                        <small>{{v.label}}</small>
                                    </h4>

                                    <span class="fa fa-plus-square player" data-toggle="tooltip" data-placement="left"
                                          title="插入发音人到指定文本位置" v-on:click="insertVoiceTag(v)"></span>
                                    <span class="fa fa-tree player" data-toggle="tooltip" data-placement="left"
                                          title="发音人支持多音字"></span>
                                </li>
                                <li v-if="checkLength()"
                                    style="text-align: center;width: 100%;display: block;line-height: 70px;color: #a2a2a2;">
                                    该分类的发音人暂时还是空的
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="select-bottom voice-speed-pitch">
                        <div class="form-title" style="margin-bottom: 0;">语速</div>
                        <div class="form-input">
                            <input id="speed" placeholder="-100 - 100" type="hidden" min="-100" max="100" step="1"
                                   value="0" data-color="#b52473" srs-none="true" class="srs">
                        </div>

                        <div class="form-title" style="margin-bottom:0;margin-top: 3px;">语调</div>
                        <div class="form-input">
                            <input id="pitch" placeholder="-100 - 100" type="hidden" min="-100" max="100" step="1"
                                   value="0" data-color="#26c1b4" srs-none="true" class="srs">
                        </div>

                        <div class="form-input text-center" style="margin-top: 25px;">
                            <button type="button" data-toggle="tooltip" data-placement="top" title="恢复为系统推荐的发音人语速/语调"
                                    class="btn btn-sm btn-primary mr5" v-on:click="resetVoiceSettings">恢复默认
                            </button>
                        </div>
                    </div>
                </section>
                <section class="voice-setting">
                    <header class="header">
                        <i class="fa fa-align-justify"></i> <span class="text">合成选项</span>
                    </header>

                    <div class="content-box" style="padding-bottom: 0;">
                        <div class="form-title">配音模式</div>
                        <div class="form-input ad-format">
                            <button type="button" data-toggle="tooltip" data-placement="top" title="仅生成配音音频"
                                    class="btn btn-sm mr5 active">普通配音
                            </button>
                        </div>
                    </div>

                    <div class="content-box">
                        <div class="form-title">音频格式</div>
                        <div class="form-input ad-format">
                            <button type="button" data-toggle="tooltip" data-placement="top" title="mp3音频，体积小、压缩格式"
                                    class="btn btn-sm mr5" v-bind:class="{ active: format == 'mp3' }"
                                    v-on:click="changeFormat('mp3')">Mp3 (双声道)
                            </button>
                            <button type="button" data-toggle="tooltip" data-placement="top"
                                    title="wav音频，体积较大、高质无损格式（限免中)" class="btn btn-sm mr5"
                                    v-bind:class="{ active: format == 'wav' }" v-on:click="changeFormat('wav')">Wav
                                (双声道)
                            </button>
                            <button type="button" data-toggle="tooltip" data-placement="top"
                                    title="PCM音频，体积小、压缩格式" class="btn btn-sm mr5"
                                    v-bind:class="{ active: format == 'wav48' }" v-on:click="changeFormat('wav48')">PCM
                                (双声道)
                            </button>
                        </div>
                    </div>
                </section>
            </div>
            <div class="task-main">
                <div class="form-input" id="editer-tips">
                    <div class="editer-content">
                        <!--编辑器预览模式-->
                        <div class="editer-view" id="content-view" style="min-height: 796px;padding: 15px 10px;height: auto;overflow-y:initial;">
                            <fieldset class="layui-elem-field">
                                <legend style="padding-bottom: 10px;margin-bottom:0;">教程文章</legend>
                                <!--<div class="layui-field-box" style="padding-top:20px;height: 357px;overflow-y: auto;">-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1715418878823835699" target="_blank">1.【赠送字符活动】2021年11月价格表</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1715417947260450469" target="_blank">2.【热门问题】如何在线充值？如何微信充值？【重点】</a></p>-->
                                <!--                                        <p><a href="https://haokan.baidu.com/v?pd=bjh&vid=8222376667113449800&fr=bjhauthor&type=video" target="_blank">3.【客户案例】推荐量一流、流量巨猛、单价30元起</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1702874117438158717" target="_blank">4.【充值】木瓜配音在哪里充值？充值方式图解</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1703145073653460112" target="_blank">5.【下载】配音下载不了？点击没有反应？</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1703146491151862714" target="_blank">6.【停顿】声音停顿感太长？声音不顺溜？如何调整？</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1703149629697151034" target="_blank">7.【换行】声音的换行停顿、语气停顿如何调节</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1702866508943109161" target="_blank">8.【教学1】木瓜声音的优势？自媒体AI声音的趋势！</a></p>-->
                                <!--                                        <p><a href="https://baijiahao.baidu.com/s?id=1702873315895056599" target="_blank">9.【教学2】如何快速挑选主播？如何试听？如何收藏？</a></p>-->
                                <!--                                        <p><a href="https://haokan.baidu.com/v?pd=bjh&vid=2157937287208879717&fr=bjhauthor&type=video" target="_blank">10.【教学2】如何快速挑选主播？如何试听？如何收藏？（视频）</a></p>-->
                                <!--                                    </div>-->
                            </fieldset>

                            <!--<hr style="width: 90%;margin: 35px auto 5px auto;"/>-->

                            <!--教程说明-->
                            <div style="padding-top:8px;">
                                <p><br></p><p>如果您遇到技术问题或配音故障，请与我们联系：</p><p><b>QQ号：1120535395</b><br>--- ---</p><p><span>木瓜配音的优势：</span></p><p>1、有将近200位主播在线可以全天24小时配音，涵盖军事、国际、时政、历史、科技、财经、娱乐、情感、影视、动漫、社会等等二十几个领域的配音需求，并且每个声音可以单独调整语速、语调、停顿、多音字等等，支持在线精细化调整声音。</p><p>2、当配音之后就需要PR剪辑视频，做字幕是非常浪费时间，木瓜配音支持配音和字幕同时下载，当你配音之后字幕也做好了，直接拖入PR里就生成了字幕，几乎是秒做字幕，省时省事又省力。</p><p>3、木瓜配音的声音非常友好，目前众多客户配音当中，无论是西瓜视频、好看视频、大鱼和企鹅号的视频，推荐量非常巨大，木瓜声音经过忧化之后全网各大平台非常友好，推荐量非常猛，流量越猛收益越高。</p><p>4、支持MCN工作室批量操作，可以同时上传几十个文案一起配音，也可以同时下载几十个配音文件，真正为各大MCN矩阵公司节约时间、提高效率。</p><p>5、支持在线原创检测，让优质的文案获取更大的平台流量。</p><p></p>                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!--    <p @click="getA">{{ message }}</p>-->
</div>

<link rel="stylesheet" href="/assets/css/page_index.css">
<link href="/static/app/api/js/srs.css" rel="stylesheet">
<script src="/assets/libs/jquery/dist/jquery.min.js"></script>
<script src="/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script type="text/javascript" src="/static/app/api/js/srs.reload.js"></script>
<script src="/static/app/api/layui-v2.6.8/layui.js"></script>
<script src="/static/app/api/js/addon_insert.js"></script>
<script src="/static/app/api/js/pinyin.dict.js"></script>


<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
    $(".navTabs-Right .tabs-mb").click(function () {
        let $this = $(this);
        let show = $this.data("show");
        if (show) {
            $(".navTabs-Right .nav").fadeOut();
            $this.data("show" , "");
        } else {
            $(".navTabs-Right .nav").fadeIn();
            $this.data("show" , "true");
        }
    });
    $("body").click(function (e) {
        let tab = $(".navTabs-Right .tabs-mb");
        let is = $(e.target).parents().is("._nav");
        if (!is && tab.data("show")) {
            $(".navTabs-Right .nav").fadeOut();
            tab.data("show" , "");
        }
    });
</script>

<script>
    new Vue({
        el: '#app',
        data: {
            voicer: '艾诚',
            voiceGroup: <?php echo $category; ?>,
            curGroup: <?php echo $selectCategoryId; ?>,
            list: <?php echo $list; ?>,
            playing: "",
            voiceParam: 'aicheng',
            format: 'mp3',
            useTextNumber: 0,
            maxTextNumber: <?php echo $site['maxTextNumber']; ?>,
            price: <?php echo $site['price']; ?>,
            defsTag: {
                tag_stop_val: 1,
                tag_stop_type: 's',
                tag_speed_val: 0,
                tag_pitch_val: 0,
                tag_alias_org_val: '',
                tag_alias_new_val: '',
            },
            playerVoice:{}
        },
        mounted() {
            $("#speed").srs();
            $("#pitch").srs();
            let $this = this;
            this.initHandleClick();
            this.playerVoice = document.getElementById("voice-player");
            this.playerVoice.addEventListener('ended', function () {
                $this.playing = "";
            }, false);
        },
        methods: {
            changeGroup(e) {
                this.curGroup = e.id
            },
            checkLength() {
                const _this = this;
                const result = this.list.some(function (item) {
                    return item.category_id == _this.curGroup
                })
                return !result
            },
            changeVoice(e) {
                this.voicer = e.name;
                this.voiceParam = e.param;
            },
            playVoice(e) {
                let $this = this;
                if (e) {
                    if($this.playerVoice.paused) {
                        $this.playerVoice.src = `/static/voice/${e.param}.wav`;
                        $this.playerVoice.play();
                        $this.playing = e.name;
                    } else {
                        $this.playerVoice.pause();
                        if ($this.playing && e.name != $this.playing) {
                            $this.playVoice(e);
                        } else {
                            $this.playing = "";
                        }
                    }
                }
            },
            resetVoiceSettings() {
                $("#speed").val(0);
                $("#pitch").val(0);
                $("#speed").srs();
                $("#pitch").srs();
            },
            changeFormat(e) {
                this.format = e
            },
            inputTextNumber(e) {
                let contentValue = $.trim($("#content").val());
                contentValue = contentValue.replace(/[{]([0-9]+)秒[}]/g, '');
                contentValue = contentValue.replace(/[{]语速(-*[0-9]+)[}]/g, '');
                contentValue = contentValue.replace(/[{]语调(-*[0-9]+)[}]/g, '');
                contentValue = contentValue.replace(/\{别名#([a-zA-Z0-9\u4e00-\u9fa5]+)=([a-zA-Z0-9\u4e00-\u9fa5]+)}/g, '$2');
                // 匹配到的字符串转换为一个长度的字符串[0]可替换任意
                contentValue = contentValue.replace(/\[pys\:([\u4e00-\u9fa5]+)\:([a-z]+[0-9])\]/, '$1');
                console.log(contentValue)
                this.useTextNumber = contentValue.length;
            },
            submitTask(isClear) {
                <?php if(!$user): ?>return layer.msg("请先登录再进行配音", {icon: 5});<?php endif; ?>

                if(this.useTextNumber < 20){
                    return layer.msg("最小配音字数为20", {icon: 5})
                }
                <?php if($user): ?>const userMoney = <?php echo $user['money']; ?>;<?php endif; ?>

                if(userMoney < this.useTextNumber * this.price){
                    layer.msg("当前余额不足以完成此次配音",{icon:5});
                }else {
                    $.post("api/index/task",{
                        voicer: this.voicer,
                        speed: $("#speed").val(),
                        pitch: $("#pitch").val(),
                        format: this.format,
                        name: $("#name").val(),
                        content: $("#content").val(),
                        wrap_stop: $("input[name='wrap-stop']:checked").val() ? true : false,
                        wrap_stop_sec: $("#wrapstopSec").val()
                    },function(res){
                        console.log(res)
                        if(res.code == 1){
                            layer.confirm('提交成功。是否前往查看任务进度？', {
                                btn: ['立即前往','取消']
                            }, function(){
                                //跳转到查看链接
                                location.href="<?php echo url('index/user/task'); ?>"
                            });
                        }else{
                            layer.msg(res.msg,{icon:5})
                        }
                    })
                }
            },
            insertTag(tag) {
                let $this = this;
                let html = $(`#tag-${tag}`).html();
                let title = $(`#tag-${tag}`).data("title");
                layui.layer.open({
                    type: 1,
                    area: ['380px', 'auto'],
                    offset: '30%',
                    title: title,
                    zIndex: 1000,
                    content: html
                });
                switch (tag) {
                    case "stop":
                        $("#tag_stop_val").val($this.defsTag.tag_stop_val);
                        $("#tag_stop_type").val($this.defsTag.tag_stop_type);
                        break;
                    case "speed":
                        $("#tag_speed_val").val($this.defsTag.tag_speed_val);
                        break;
                    case "pitch":
                        $("#tag_pitch_val").val($this.defsTag.tag_pitch_val);
                        break;
                    case "alias":
                        let selectWord = window.getSelection();
                        if ($.trim(selectWord) != "") {
                            $("#tag_alias_org_val").val(selectWord);
                            $("#tag_alias_new_val").val("");
                        } else {
                            $("#tag_alias_org_val").val($this.defsTag.tag_alias_org_val);
                            $("#tag_alias_new_val").val($this.defsTag.tag_alias_new_val);
                        }
                        break;
                }
            },
            insertVoiceTag(e) {
                const $this = this;
                console.log(e.name)
                $("#content").insertAtCaret(`{@${e.name}}`);
            },
            initHandleClick() {
                const $this = this;
                $("body").delegate(".tag-submit", "click", function () {
                    let tag = $(this).data("tag");
                    let v, vt;
                    let vs, vn, vg;
                    switch (tag) {
                        case "stop":
                            v = $("#tag_stop_val").val();
                            vt = $("#tag_stop_type").val();
                            v = parseInt(v);
                            if (vt == "s") {
                                if (v > 60) v = 60;
                                if (v < 0) v = 1;
                                $("#content").insertAtCaret(`{${v}秒}`);
                            } else {
                                if (v > 60000) v = 60000;
                                if (v < 10) v = 10;
                                $("#content").insertAtCaret(`{${v}毫秒}`);
                            }
                            $this.defsTag.tag_stop_val = v;
                            $this.defsTag.tag_stop_type = vt;
                            break;
                        case "speed":
                            v = parseInt($("#tag_speed_val").val());
                            if (v > 100) v = 100;
                            if (v < -100) v = -100;
                            $("#content").insertAtCaret(`{语速${v}}`);
                            $this.defsTag.tag_speed_val = v;
                            break;
                        case "pitch":
                            v = parseInt($("#tag_pitch_val").val());
                            if (v > 100) v = 100;
                            if (v < -100) v = -100;
                            $("#content").insertAtCaret(`{语调${v}}`);
                            $this.defsTag.tag_pitch_val = v;
                            break;
                        case "alias":
                            vs = $.trim($("#tag_alias_org_val").val());
                            vn = $.trim($("#tag_alias_new_val").val());
                            if (vs == "") {
                                return layer.msg("原始文本不能为空",{icon:"5"})
                            }
                            $("#content").insertAtCaret(`{别名#${vs}=${vn}}`);
                            $this.defsTag.tag_alias_org_val = vs;
                            $this.defsTag.tag_alias_new_val = vn;
                            break;
                    }
                    layer.closeAll("page");
                    console.log(tag)
                })
                $("body").delegate(".polyphone-container .pys", "click", function () {
                    let pys = $(this).data("pys");
                    let han = $(this).data("han");
                    if (!pys || !han) {
                        return;
                    }
                    $("#content").insertAtCaret(`[pys:${han}:${pys}]`);
                    layer.closeAll();
                });
            },
        }
    })
</script>
<script>
    var chineseCharToPinyin = (function () {
        // 注意这里的变量 pinyinDict 来自于字典文件，需要先引入字典文件，且该变量不要在程序中被覆盖
        var temp = pinyinDict.split(','), withtone = {};
        // 循环遍历字典，以汉字字符为 key，对应的拼音为 value，形成一个超大的 JSON 对象
        for (var i = 0; i < temp.length; i++) {
            withtone[String.fromCharCode(i + 19968)] = temp[i];
        }
        var toneMap = {
            "ā": "a1", "á": "a2", "ǎ": "a3", "à": "a4", "ō": "o1", "ó": "o2", "ǒ": "o3",
            "ò": "o4", "ē": "e1", "é": "e2", "ě": "e3", "è": "e4", "ī": "i1", "í": "i2",
            "ǐ": "i3", "ì": "i4", "ū": "u1", "ú": "u2", "ǔ": "u3", "ù": "u4", "ü": "v0",
            "ǖ": "v1", "ǘ": "v2", "ǚ": "v3", "ǜ": "v4", "ń": "n2", "ň": "n3", "": "m2"
        };
        // 未开启声调识别的时候，将带声调的字符切换成不带声调的字母
        var removeTone = function (pinyin) {
            console.log("拼音",pinyin)
            return pinyin.replace(/[āáǎàōóǒòēéěèīíǐìūúǔùüǖǘǚǜńň]/g, function (m) {
                return toneMap[m];
            });
        };
        // 过滤掉多音字穷举产生的相同结果
        var simpleUnique = function (array) {
            var result = [], hash = {};
            for (var i = 0; i < array.length; i++) {
                var key = (typeof array[i]) + array[i];
                if (!hash[key]) {
                    result.push(array[i]);
                    hash[key] = true;
                }
            }
            return result;
        };
        // 多音字穷举
        var handlePolyphone = function (array, joinChar) {
            var result = [''], temp = [];
            for (var i = 0; i < array.length; i++) {
                temp = [];
                var t = array[i].split(' ');
                for (var j = 0; j < t.length; j++) {
                    for (var k = 0; k < result.length; k++)
                        temp.push(result[k] + (result[k] ? joinChar : '') + t[j]);
                }
                result = temp;
            }
            return simpleUnique(result);
        };
        // 对外返回一个转化的方法
        return function (str, isTone = false, isMultitone = false) {
            var result = [], pinyin;
            for (var i = 0; i < str.length; i++) {
                pinyin = withtone[str[i]];
                console.log(pinyin)
                if (!isTone && pinyin) pinyin = removeTone(pinyin);
                if (!isMultitone && pinyin) pinyin = pinyin.replace(/ .*$/g, '');
                result.push(pinyin || str[i]);
            }
            if (!isMultitone) return result.join(" ");
            return handlePolyphone(result, " ");
        }
    })();
</script>
    </main>

    <footer class="footer clearfix">
        <p class="copyright">Copyright&nbsp;©&nbsp;<?php echo date("Y"); ?> <?php echo htmlentities($site['name']); ?> All Rights Reserved <a href="https://beian.miit.gov.cn" target="_blank"><?php echo htmlentities($site['beian']); ?></a></p>
    </footer>
</div>
</body>

</html>
