<?php

namespace app\index\controller;

use app\common\controller\Frontend;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'default';

    public function index()
    {
        $categoryModel = new \app\common\model\voice\Category;
        $categoryInfo = $categoryModel->select();

        $listsModel = new \app\common\model\voice\Lists;
        $listsInfo = $listsModel->where('status',1)->select();

        $this->view->assign('title', __('Index'));
        $this->view->assign('category', json_encode($categoryInfo));
        $this->view->assign('selectCategoryId', $categoryInfo[0]->id??0);
        $this->view->assign('list', json_encode($listsInfo));
        return $this->view->fetch();
    }

    public function tutorial(){
        $categoryModel = new \app\common\model\voice\Category;
        $categoryInfo = $categoryModel->select();

        $listsModel = new \app\common\model\voice\Lists;
        $listsInfo = $listsModel->where('status',1)->select();

        $this->view->assign('title', "教程");
        $this->view->assign('category', json_encode($categoryInfo));
        $this->view->assign('selectCategoryId', $categoryInfo[0]->id??0);
        $this->view->assign('list', json_encode($listsInfo));
        return $this->view->fetch();
    }

}
