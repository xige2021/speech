<?php

return [
    'Id'         => 'ID',
    'Order_no'   => '任务编号',
    'User_id'    => '用户',
    'Name'       => '任务名称',
    'Voicer'     => '配音员',
    'Pay_money'  => '消费金额',
    'Status'     => '状态',
    'Status 0'   => '进行中',
    'Status 1'   => '已完成',
    'Down_url'   => '下载链接',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
